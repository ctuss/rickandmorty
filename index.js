'use strict'

/**
 * Dependencies
 * @ignore
 */
const express = require('express')
const path = require('path');
/**
 * App
 * @ignore
 */
const _app_folder = 'build';
const app = express()

app.get('*.*', express.static(_app_folder, { maxAge: '1y'}));

app.all('*', function(req, res) {
    res.status(200).sendFile(`/`, { root: _app_folder});
})


app.listen(process.env.PORT || 4000, () =>
    console.log(`Listening on port ${process.env.port || 3000}`)
);