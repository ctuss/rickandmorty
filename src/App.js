import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import CharacterList from './CharacterList/CharacterList.js'
import CharacterProfile from './CharacterProfile/CharacterProfile'



class App extends React.Component {
  state = {
    count: null,
    next: null,
    characters: [
    ],
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/character/';
    const response = await fetch(url);
    const json = await response.json();
    await this.setState({
      count: json.info.count,
      next: json.info.next,
      characters: json.results
    });
  }

  async getNextCharacters() {
    const url = this.state.next;
    const response = await fetch(url);
    const json = await response.json();

    await this.setState({
      next: json.info.next,
      characters: this.state.characters.concat(json.results)
    });
  }

  render() {
    return (
      <div id="root">
        <div id="header">
        </div>
        <div id="pageBody">
          <Router>
            <Switch>
              <Route exact path="/">
                <input class="form-control" id="searchBar" type="text" placeholder="Default input"></input>
                <CharacterList characters={this.state.characters} />
                <div id="loadMoreButton">
                  <button onClick={() => this.getNextCharacters()}>Load More</button>
                </div>
              </Route>
              <Route exact path="/profile/:id">
                <CharacterProfile />
              </Route>
              <Route component={NoMatch}></Route>
            </Switch>
          </Router>
        </div>
      </div>
    )
  }
}

const NoMatch = () => {
  return <div>404, not found</div>
}

export default App;