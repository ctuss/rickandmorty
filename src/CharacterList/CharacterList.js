import React from 'react';
import CharacterCard from '../CharacterCard/CharacterCard.js'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import './characterlist.css';

class CharacterList extends React.Component {
    render() {
        console.log(this.props.characters)
        return (
            <div id="cards">
                <div id="flex-container">
                    {this.props.characters.map((character, i) =>
                        <div id="cardTest">
                            <Link to={`/profile/${character.id}`}>
                                <CharacterCard key={i} character={character} />
                            </Link>
                        </div>
                    )}
                </div>
            </div>
        )
    }
}

export default CharacterList