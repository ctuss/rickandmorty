import React from 'react';
import { withRouter } from 'react-router';

class CharacterProfile extends React.Component {
    state = {
        id: null,
        character: {}
    }
    componentDidMount() {
       this.getCharacterProfile();
    }

    async getCharacterProfile() {
        const url = 'http://localhost:8080/api/character/' + this.props.match.params.id;
        const response = await fetch(url);
        const json = await response.json();
        this.setState({
            character: json
        })
    }

    render() {
        return (
            <div>
                {this.props.match.params.id} {this.state.character.name}
            </div>
        )
    }
}

export default withRouter(CharacterProfile)