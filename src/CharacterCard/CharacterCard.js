import React from 'react';

import './charactercard.css';

class CharacterCard extends React.Component {
    render() {
        return (
            <div id="card">
                <div>
                    <img src={this.props.character.image} alt="lol" id="characterPhoto" />
                    <div>
                        <p>{this.props.character.name}</p>
                        <ul id="characterStats">
                            <li><b>Status</b><span>{this.props.character.status}</span></li>
                            <li><b>Species</b><span>{this.props.character.species}</span></li>
                            <li><b>Origin</b><span>{this.props.character.origin.name}</span></li>
                            <li><b>Last location</b><span>{this.props.character.location.name}</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default CharacterCard