FROM node:lts-alpine AS builder

WORKDIR /src
COPY package.json .
RUN npm install
COPY . .

RUN npm run build
# /src/dist

FROM node:lts-alpine AS production

ENV PORT 4200

WORKDIR /app
COPY package.json .
RUN npm install express path
COPY index.js .
COPY --from=builder /src/dist ./dist

EXPOSE 4200/tcp

CMD [ "node", "index.js" ]